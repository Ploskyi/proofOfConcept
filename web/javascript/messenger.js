const baseUrl = location.origin;
const celebratorEmailId = 'celebratorEmail';
const venueEmailId = 'venueEmail';
const celebratorTextId = 'celebratorText';
const venueTextId = 'venueText';

const get = (url) => {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();
        request.open('GET', url);
        request.onload = () => {
            if (request.status === 200) {
                resolve(request.response);
            }
            else {
                reject(Error(request.statusText));
            }
        };
        request.onerror = () => {
            reject(Error("Network Error"));
        };
        request.send();
    });
};
const getValue = (id) => {
    return document.getElementById(id).value;
};

const getAllMessagesUrl = (senderEmail, recipientEmail) => {
    return `${baseUrl}/getAll?senderEmail=${senderEmail}&recipientEmail=${recipientEmail}`;
};
const getSendUrl = (senderEmail, recipientEmail, text) => {
    return `${baseUrl}/addMessage?senderEmail=${senderEmail}&recipientEmail=${recipientEmail}&text=${text}`;
};

function sendCelebrator() {
    sendRequest(getSendUrl(getValue(celebratorEmailId), getValue(venueEmailId), getValue(celebratorTextId)));
}

function sendVenue() {
    sendRequest(getSendUrl(getValue(venueEmailId), getValue(celebratorEmailId), getValue(venueTextId)));
}

const getClass = (message, venueEmail) => {
    return message.senderEmail === venueEmail ? 'venueMessage' : 'celebratorMessage';
};

const getMessage = (message, venueEmail) => {
    return `<div class="${getClass(message, venueEmail)}"><span>${message.text}</span></div>`
};

const getMessagesList = (messages) => {
    return messages.map(message => getMessage(message, getValue(venueEmailId))).reduce((result, message) => result + message, '');
};

const loadMessages = () => {
    get(getAllMessagesUrl(getValue(venueEmailId), getValue(celebratorEmailId))).then(response => {
        document.getElementById('threadText').innerHTML = getMessagesList(JSON.parse(response));
    });
};

const sendRequest = (url) => {
    get(url).then(response => {
        document.getElementById(venueTextId).value = '';
        document.getElementById(celebratorTextId).value = '';
        loadMessages();
    }, (error) => {
    });
};
window.onload = loadMessages;