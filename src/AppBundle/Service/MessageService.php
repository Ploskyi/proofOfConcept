<?php

namespace AppBundle\Service;

use AppBundle\Entity\Message;
use Doctrine\ORM\EntityManager;

class MessageService
{

    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function addMessage($senderEmail, $recipientEmail, $text)
    {
        if ($text == '') {
            return;
        }
        $message = new Message();
        $message->setText($text);
        $message->setDate(new \DateTime());
        $message->setSenderEmail($senderEmail);
        $message->setRecipientEmail($recipientEmail);
        $this->em->persist($message);
        $this->em->flush();
    }

    public function getAll($venueEmail, $celebratorEmail)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('m')
            ->from('AppBundle\Entity\Message', 'm')
            ->where($qb->expr()->orX(
                $qb->expr()->andX(
                    $qb->expr()->eq('m.senderEmail', '?1'),
                    $qb->expr()->eq('m.recipientEmail', '?2')
                ),
                $qb->expr()->andX(
                    $qb->expr()->eq('m.senderEmail', '?2'),
                    $qb->expr()->eq('m.recipientEmail', '?1')
                )
            ))
            ->orderBy('m.date', 'ASC');
        $qb->setParameters(array(1 => $venueEmail, 2 => $celebratorEmail));
        return $qb->getQuery()->getArrayResult();
    }
}