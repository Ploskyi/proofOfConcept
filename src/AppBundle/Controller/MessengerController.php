<?php

namespace AppBundle\Controller;

use Mailgun\Exception\HttpClientException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MessengerController extends Controller
{
    /**
     * @Route("/messenger", name="messenger")
     */
    public function indexAction(Request $request)
    {
        return $this->render('messenger/messenger.html');
    }

    /**
     * @Route("/addMessage", name="addMessage")
     */
    public function addMessage(Request $request)
    {
        $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        $recipientEmail = $request->query->get('recipientEmail');
        $senderEmail = $request->query->get('senderEmail');
        $text = $request->query->get('text');
        try {
            $this->get('mail_service')->sendNotification($recipientEmail, $senderEmail, $text, $baseUrl);
        } catch (HttpClientException $e) {
            $this->get('logger')->error('error sending email notification ' . $e->getMessage());
        }
        return new Response($this->get('message_service')->addMessage($senderEmail, $recipientEmail, $text));
    }

    /**
     * @Route("/getAll", name="getAll")
     */
    public function getAll(Request $request)
    {
        return new JsonResponse($this->get('message_service')->getAll($request->query->get('senderEmail')
            , $request->query->get('recipientEmail')));
    }

}