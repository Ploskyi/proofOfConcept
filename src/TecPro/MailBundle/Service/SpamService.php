<?php

namespace TecPro\MailBundle\Service;
require '../vendor/autoload.php';

class SpamService
{

    private $spamWords;

    public function __construct()
    {
        $this->spamWords = file(__DIR__.'/../Resources/spam.txt', FILE_IGNORE_NEW_LINES);
    }

    public function getSpamScore($test)
    {
        $post = [
            'email' => $test,
            'options' => 'short'
        ];
        $ch = curl_init('http://spamcheck.postmarkapp.com/filter');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode(json_decode($response, true)['score']);
    }

    public function getCountOfSpamWords($text)
    {
        $text = strtoupper($text);
        $countOfSpamWords = 0;
        foreach ($this->spamWords as $key => $value) {
            $countOfSpamWords += substr_count($text, strtoupper($value));
        }
        return $countOfSpamWords;
    }

}