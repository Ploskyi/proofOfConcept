<?php

namespace TecPro\MailBundle\Service;
require '../vendor/autoload.php';

use Mailgun\Mailgun;

class MailService
{
    private $mg;
    private $spamService;

    public function __construct(SpamService $spamService)
    {
        $this->mg = Mailgun::create('key-343b54d980ec0ddde591738e7d38b8a8');
        $this->spamService = $spamService;
    }

    public function sendNotification($to, $from, $text, $baseUrl)
    {
        $spamScore = $this->spamService->getSpamScore($text);
        $spamWordsCount = $this->spamService->getCountOfSpamWords($text);
        $this->mg->messages()->send('sandbox7fa88b3af93c41308174561625a2cc98.mailgun.org', [
            'from' => 'venue@sandbox7fa88b3af93c41308174561625a2cc98.mailgun.org',
            'to' => $to,
            'subject' => $from . ' sent you new message in venue report messenger',
            'html' => $this->getForm($to, $from, $text, $baseUrl, $spamScore, $spamWordsCount),
            'text' => $text
        ]);
    }

    private function getForm($to, $from, $text, $baseUrl, $spamScore, $spamWordsCount)
    {
        $form = str_replace("\${messageText}", $text, file_get_contents('form.html'));
        $form = str_replace("\${respondUrl}", $baseUrl . '/addMessage', $form);
        $form = str_replace("\${senderEmail}", $to, $form);
        $form = str_replace("\${spamScore}", $spamScore, $form);
        $form = str_replace("\${spamWordsCount}", $spamWordsCount, $form);
        return str_replace("\${recipientEmail}", $from, $form);
    }

}