<?php

namespace TecPro\MailBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MailController extends Controller
{
    /**
     * @Route("/emailReply", name="emailReply")
     * @Method("POST")
     */
    public function emailReply(Request $request)
    {
        $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        $requestValue = $request->request;
        $recipient = str_replace("sent you new message in venue report messenger", '', $requestValue->get('subject'));
        $recipient = str_replace("Re: ", '', $recipient);
        $this->get('mail_service')->sendNotification($recipient, $requestValue->get('sender'), $requestValue->get('stripped-text'), $baseUrl);
        $this->get('message_service')->addMessage($requestValue->get('sender'), $recipient, $requestValue->get('stripped-text'));
        return new Response();
    }

}